var expect = require('chai').expect;
process.env.FLIPBOARD_TEST=true;

var src;


describe('Maze Client', function() {
    beforeEach(function() {
    	src = require('../client');
    });

	describe('self.ask', function() {
    	it('updates map to keep history of places travelled', function() {
    		var r = {
    			map: [],
    			adjacent: []
    		};
    		src.ask(r,1,2);
    		expect(r.map[2][1]).to.equal(r);
    	});

    	it('moves in every adjacent step', function(next) {
    		var map = [];
    		var r = {
    			map: map,
    			adjacent: [{
	    			x:1,
	    			y:0
    			},{
	    			x:4,
	    			y:0
    			},
    			]
    		};
    		var callCount = 0;
    		src.g = function(url, cb){
    			callCount++;
    			var vals = url.split('&');
    			if(callCount === 1) {
    				expect(vals[1]).to.equal('x=1');
    				expect(vals[2]).to.equal('y=0');
    			}

    			if(callCount === 2){
    				expect(vals[1]).to.equal('x=4');
    				expect(vals[2]).to.equal('y=0');
    				src.g = null;
    				next();
    			}

    		};

    		src.ask(r,0,0);
    	});

    	it('does not move if it has already travelled there', function(next) {
    		var map = [];
    		var r = {
    			map: map,
    			adjacent: [{
	    			x:0,
	    			y:0,
    			},
				{
	    			x:1,
	    			y:0
    			}
    			]
    		};
    		var callCount = 0;
    		src.g = function(url, cb){

    			callCount++;
    			var vals = url.split('&');

    			if(callCount === 1){
    				setTimeout(function(){
    					expect(callCount).to.equal(1);
    					next();
    				});
    			}
    		};
    		src.ask(r,0,0);
    	});
	});
});