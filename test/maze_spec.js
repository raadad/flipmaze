var expect = require('chai').expect;
process.env.FLIPBOARD_TEST=true;

var src;




describe('Maze logic', function() {
    beforeEach(function() {
    	src = require('../maze');
    });

	describe('self.generateKeyMap', function() {
    	it('generates a valid key map', function() {
            var map = src.generateKeyMap(4,4);
            expect(map[3][3]).to.equal('n');
            expect(map[2][2]).to.equal('k');
    	});
	});

    describe('self.notEdge', function() {
        it('Handles North', function() {
            expect(src.notEdge(0,0,"north",testMaze)).to.equal(undefined);
            expect(src.notEdge(0,1,"north",testMaze)).to.equal(true);
        });
        it('Handles South', function() {
            expect(src.notEdge(0,0,"south",testMaze)).to.equal(true);
            expect(src.notEdge(4,4,"south",testMaze)).to.equal(undefined);
        });
        it('Handles West', function() {
            expect(src.notEdge(0,0,"west",testMaze)).to.equal(undefined);
            expect(src.notEdge(1,1,"west",testMaze)).to.equal(true);
        });
        it('Handles East', function() {
            expect(src.notEdge(4,4,"east",testMaze)).to.equal(undefined);
            expect(src.notEdge(1,1,"east",testMaze)).to.equal(true);
        });
    });

    describe('self.getDirection', function() {
        it('Handles East', function() {
            expect(src.getDirection(0,0,"e",testMaze)).to.equal('east');
        });
        it('Handles East', function() {
            expect(src.getDirection(1,0,"f",testMaze)).to.equal('west');
        });
        it('Handles North', function() {
            expect(src.getDirection(0,1,"f",testMaze)).to.equal('north');
        });
        it('Handles South', function() {
            expect(src.getDirection(0,0,"k",testMaze)).to.equal('south');
        });
    });

    describe('self.validate', function() {
        it('Can solve a maze', function(next) {
            src.validate('edinsrqv',testMaze,0,0,function(code, guess, x, y){
                expect(code).to.equal(4);
                next();
            });
        });
        it('Cannot move in that direction', function(next) {
            src.validate('kdinsrqv',testMaze,0,0,function(code, guess, x, y){
                expect(code).to.equal(3);
                next();
            });
        });
        it('Cannot determine direction', function(next) {
            src.validate('edinsrqg',testMaze,0,0,function(code, guess, x, y){
                expect(code).to.equal(2);
                next();
            });
        });
        it('Still in Maze', function(next) {
            src.validate('edins',testMaze,0,0,function(code, guess, x, y){
                expect(code).to.equal(1);
                next();
            });
        });
    });


});


var testMaze =  {
  x: 5, y: 5,
  horiz:
   [ [ 1, 1, 1 ],
     [ , , , 1 ],
     [ 1, , , 1 ],
     [ 1, , 1, 1 ],
     [ 1, 1, 1, 1 ]],
  verti:
   [ [ , 1, 1, 1, 1 ],
     [ 1, , 1, , 1 ],
     [ , 1, 1 ],
     [ 1, , , , 1 ],
     [],
     ],
   map:[
    [ 'f', 'e', 'd', 'c', 'b' ],
    [ 'k', 'j', 'i', 'h', 'g' ],
    [ 'p', 'o', 'n', 'm', 'l' ],
    [ 'u', 't', 's', 'r', 'q' ],
    [ 'z', 'y', 'x', 'w', 'v' ] ]
};