build: clean install compile lint unit bundle

clean:
	@rm -fr ./app-*.tar
	@rm -fr ./node_modules

install:
	@npm install

compile:
	./node_modules/coffee-script/bin/coffee -c server.coffee
	./node_modules/coffee-script/bin/coffee -c client.coffee
lint:
	./node_modules/coffeelint/bin/coffeelint *.coffee
unit: compile
	./node_modules/mocha/bin/mocha

bundle:
	@tar cf app-build.tar *