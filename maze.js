var self = {};

self.create = function(x, y) { // Generates the maze
	var n = x * y - 1;
	if (n < 0) {
		console.log("illegal maze dimensions");
		return;
	}
	var horiz = [];
	for (var j = 0; j < x + 1; j++) horiz[j] = [],
		verti = [];
	for (var j = 0; j < x + 1; j++) verti[j] = [],
		here = [Math.floor(Math.random() * x), Math.floor(Math.random() * y)],
		path = [here],
		unvisited = [];
	for (var j = 0; j < x + 2; j++) {
		unvisited[j] = [];
		for (var k = 0; k < y + 1; k++)
			unvisited[j].push(j > 0 && j < x + 1 && k > 0 && (j != here[0] + 1 || k != here[1] + 1));
	}
	while (0 < n) {
		var potential = [
			[here[0] + 1, here[1]],
			[here[0], here[1] + 1],
			[here[0] - 1, here[1]],
			[here[0], here[1] - 1]
		];
		var neighbors = [];
		for (var j = 0; j < 4; j++)
			if (unvisited[potential[j][0] + 1][potential[j][1] + 1])
				neighbors.push(potential[j]);
		if (neighbors.length) {
			n = n - 1;
			next = neighbors[Math.floor(Math.random() * neighbors.length)];
			unvisited[next[0] + 1][next[1] + 1] = 0;
			if (next[0] == here[0])
				horiz[next[0]][(next[1] + here[1] - 1) / 2] = 1;
			else
				verti[(next[0] + here[0] - 1) / 2][next[1]] = 1;
			path.push(here = next);
		} else
			here = path.pop();
	}
	return {
		x: x,
		y: y,
		horiz: horiz,
		verti: verti,
		map: self.generateKeyMap(x,y)
	};
};

self.display = function(m) { //Drwas an ascii representation of the maze
	var text = [];
	for (var j = 0; j < m.x * 2 + 1; j++) {
		var line = [];
		if (0 === j % 2)
			for (var k = 0; k < m.y * 4 + 1; k++)
				if (0 == k % 4)
					line[k] = '+';
				else
		if (j > 0 && m.verti[j / 2 - 1][Math.floor(k / 4)])
			line[k] = ' ';
		else
			line[k] = '-';
		else
			for (var k = 0; k < m.y * 4 + 1; k++)
				if (0 == k % 4)
					if (k > 0 && m.horiz[(j - 1) / 2][k / 4 - 1])
						line[k] = ' ';
					else
						line[k] = '|';
		else
			line[k] = ' ';
		if (0 == j) line[1] = line[2] = line[3] = ' ';
		if (m.x * 2 - 1 == j) line[4 * m.y] = ' ';
		text.push(line.join('') + '\r\n');
	}
	return text.join('');
};

self.generateKeyMap = function(sizeX,sizeY){ // generates a repeatable charachter mapping of x y co-ordinates
	var map = [];
	var count = 0;
	for(var y=0; y<sizeY; y++){
		if(!map[y]) map[y] = [];
		for(var x=sizeX-1; x >= 0; x--){
			count++;
			map[y][x] = 'abcdefghijklmnopqrstuvwxyz'[count%26];
		}
	}
	return map;
};

self.notEdge = function(x,y,d,m){ //Used to dermine if moving in a direction will fall outside of the bounds of the map
	if(d === 'north' && y > 0) return true;
	if(d === 'east' && x < m.x-1) return true;
	if(d === 'south' && y < m.y-1) return true;
	if(d === 'west' && x > 0) return true;
};

self.move = function(x,y,d,m){ //returns wheather a move in a certain direction is valid
	if(!self.notEdge(x,y,d,m)) return false;
	if(d === 'north' && m.verti[y-1] && m.verti[y-1][x]) return true;
	if(d === 'east' &&  m.horiz[y][x]) return true;
	if(d === 'south' && m.verti[y][x]) return true;
	if(d === 'west' && m.horiz[y][x-1]) return true;
	else return false;
};

self.getDirection = function(x,y,l,m){ // will determine what direction a specific letter is in based on a position
	if(x > 0 && m.map[y][x-1] === l) return 'west';
	if(x < m.x-1 && m.map[y][x+1] === l) return 'east';
	if(y < m.y-1 && m.map[y+1][x] === l) return 'south';
	if(y > 0 && m.map[y-1][x] === l) return 'north';
};

self.validate = function(guess, m, x,y,done){ // recursivly walks the maze based on a charachter sequence
	if(!guess) return done(1,guess, x,y); //Still in Maze

	var d = self.getDirection(x,y,guess[0],m);

	if(!d) return done(2, guess, x,y); //Could not determine direction
	if(!self.move(x,y,d,m)) return done(3,guess, x,y); //Could not move in specified direction

	if(d === 'north') y-=1;
	if(d === 'east') x+=1;
	if(d === 'south') y+=1;
	if(d === 'west') x-=1;

	if(self.atEnd(x,y,m)) return done(4, guess, x,y); //Solution is correct

	self.validate(guess.slice(1), m, x,y, done);
};

self.atEnd = function(x,y,m) { // exit's on all mazes are in the bottom right corner of the maze
	return (x === m.x-1 && y === m.y-1);
};


self.getPosition = function(x,y,m){ //returns representation of a specific position in the maze and the moves that are possible from that point
	var res = {
		letter: m.map[y][x],
		end : self.atEnd(x,y,m),
		adjacent: []
	};

	if(self.move(x,y,'east',m)) res.adjacent.push({x:x+1, y:y});
	if(self.move(x,y,'west',m)) res.adjacent.push({x:x-1, y:y});
	if(self.move(x,y,'north',m)) res.adjacent.push({x:x, y:y-1});
	if(self.move(x,y,'south',m)) res.adjacent.push({x:x, y:y+1});

	return res;
};


module.exports = self;