request = require("request")
yargs = require("yargs")
Canvas = require('drawille')

baseUrl =  yargs.argv._[0] || "http://localhost:3000/"

# state and function mapping
self = {}
self.g = null # Request get function
self.completed = null # Tracks completion of maze
self.s = null # Stores maze identifier


c = new Canvas(50,50) #Used to show visual progress through maze


self.step = (prev, move) -> #Makes request to server and links previous state
  url = baseUrl+"step?s="+self.s+"&x="+move.x+"&y="+move.y
  self.g(url, (err, res) ->
    return step(prev, move) && console.log err if err
    r = JSON.parse(res.body)
    r.prev = prev
    r.map = prev.map
    self.ask(r,move.x,move.y)
  )

self.start = -> # Entry point
  self.g = request.get # makes request.get mockable in unit tests
  self.completed = false
  self.g baseUrl+'start', (err, res) ->
    return self.start() && console.log err if err # If there is a connection error just try again :|
    self.s = res.req.path.split('?s=')[1].split('&')[0] # Extract maze identifier from url
    r = JSON.parse(res.body)
    r.prev = {map:[]} # data structure relies on the existance of a prev position
    r.map = r.prev.map # reponse attaches map
    self.ask(r,0,0) # stars the walking loop
  self.draw() # Starts rendering loop


self.ask = (r, x, y) -> # BFS walk through maze
  if(self.completed) then return # Async requests should not keep firing if maze is finished
  r.map[y] = [] unless r.map[y] # Needed to stop errors8 with undefined access
  r.map[y+1] = [] unless r.map[y+1] # Same as above
  r.map[y][x] = r # Marks that position has already been travelled

  c.set(x, y) # places pixel to represent position visually

  if r.end then return self.check(r, "", self.s) # If the server advises that the end has been reached, check that is so

  r.adjacent.forEach (move) -> # Asnychronously walk all adjacent positions
    return if r.map[move.y][move.x] #Stops duplicate moves
    self.step(r, move)


self.check = (r, path, id) -> #Recursivly builds check charachter sequence
  self.completed = true
  path = path+r.letter if r.letter
  return self.check(r.prev, path, id) if r.prev # Wont continue untill backtrac is completed

  guess = path.split('').reverse().join('') #Reverses the order of the sequence as it is built backwards
  console.log("Guess is: "+guess+" for: "+ id)
  self.g(baseUrl+'check?s='+id+'&guess='+guess, (err,res) ->
    console.log(res.body) #End game!
  )

self.draw = ->
  return if self.completed
  process.stdout.write(c.frame())
  setTimeout(self.draw, 1000/24)

unless process.env.FLIPBOARD_TEST then setTimeout(self.start, 1000) #allows script to be loaded without execution for unit tests

module.exports = self
