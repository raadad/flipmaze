# Imports
Maze = require('./maze')
express = require("express")
redis = require("redis")
fs = require("fs")

# Varibale and connection initilisation
client = redis.createClient(
  {host:process.env.FLIPMAZE_REDISHOST || 'localhost'})

app = express()
log = console.log

port =  process.env.FLIPMAZE_HTTP_PORT || 3000

mazeSizeX = 30
mazeSizeY = 30
mazeCache = {}


# Utility Functions
getMaze = (id, cb) -> # Retrieves maze - first checks in memory cache, then checks redis
  if mazeCache[id] then return cb(null, mazeCache[id])
  client.get id, (err, reply) ->
    return cb(err) if err
    return cb({message:"Maze cannot be found"}) unless reply
    cb(null,JSON.parse(reply))


#Routes

app.get '/healthcheck', (req, res) ->  #Used to determine if server is still running by AWS
  res.json({status:"ok"})

app.get '/', (req, res) -> # Index Page
  try res.end(fs.readFileSync("./readme.md"))
  catch e
    res.json e

app.get '/start', (req, res) ->  #Generates a new maze, saves it into cache's and then redirects to /step
  maze = Maze.create(mazeSizeX,mazeSizeX)
  maze.id = Date.now()
  log(Maze.display(maze))
  client.set(maze.id+"",JSON.stringify(maze))
  log('New map created: %s', maze.id)
  res.redirect(301, '/step?s='+maze.id+'&x=0&y=0')

app.get '/step', (req, res) -> #Retrieves information about given x, y, coordinate
  return res.json({message:"maze id missing (s)"}) unless req.query.s
  return res.json({message:"maze x pos missing (x)"}) unless req.query.x
  return res.json({message:"maze y pos missing (y)"}) unless req.query.y

  maze = getMaze req.query.s, (err, maze) ->
    return res.end(err) if err
    res.json(Maze.getPosition(
      parseInt(req.query.x), parseInt(req.query.y),maze, true))

app.get '/check', (req, res) -> #Walks the maze based on the x\xymapping of the letter sequence and return result - used to check solutions
  return res.json({message:"maze id missing (s)"}) unless req.query.s
  return res.json({message:"guess id missing (guess)"}) unless req.query.guess

  getMaze req.query.s, (err, maze) ->
    return res.json(err) if err
    log("checking: "+ req.query.guess)
    Maze.validate req.query.guess.slice(1), maze, 0,0,
      (code, guess, x, y) ->
        result = {
          solution: false,
          steps: req.query.guess.length,
          map: req.query.s
          x:x,
          y:y,
          message: "Good Job!"
        }

        if code == 4 then (
          result.solution = true and log("Maze #{result.map} solved!"))
        if code == 3 then (
          result.message  = "Could not move in specified direction")
        if code == 2 then (
          result.message  = "Could not determine direction")
        if code == 1 then (
          result.message  = "Still in Maze")

        return res.json(result)

#Run

app.listen port
log "Maze server listening on port %d in %s mode", port, app.settings.env